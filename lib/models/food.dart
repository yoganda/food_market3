part of 'models.dart';

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, picturePath, description, ingredients, price, rate];
}

List<Food> mockFoods = [
  Food(
      id: 1,
      picturePath: "assets/food/b1.jpg",
      name: "BanuGGet",
      description: "Banugget Original",
      ingredients: "pisang,susu",
      price: 15000,
      rate: 4.2),
  Food(
      id: 2,
      picturePath: "assets/food/b2.jpg",
      name: "BanuGGet",
      description: "Banugget Tiramisu",
      ingredients: "pisang,susu",
      price: 20000,
      rate: 4.4),
  Food(
      id: 3,
      picturePath: "assets/food/b3.jpg",
      name: "BanuGGet",
      description: "Banugget Ovomaltine",
      ingredients: "pisang,susu",
      price: 25000,
      rate: 4.9),
  Food(
      id: 4,
      picturePath: "assets/food/b4.jpg",
      name: "BanuGGet",
      description: "Banugget ChocoCheese",
      ingredients: "pisang,susu",
      price: 20000,
      rate: 4.5),
  Food(
      id: 5,
      picturePath: "assets/food/b5.jpg",
      name: "BanuGGet",
      description: "Banugget Chocomaltine",
      ingredients: "pisang,susu",
      price: 20000,
      rate: 4.3),
  Food(
      id: 6,
      picturePath: "assets/food/b6.jpg",
      name: "BanuGGet",
      description: "Banugget Matcha",
      ingredients: "pisang,keju",
      price: 22000,
      rate: 4.5),
];
